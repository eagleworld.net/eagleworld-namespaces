Eagleworld.net Namespaces
=========================

This small repo sets up the namespaces used by all Eagleworld.net services.

So far, this includes the following namespaces:

- eagleworld - Production environment services
- ew-staging - Staging environment services
- ew-dev - Development services

Prod and Staging are to be used primarily on Production-grade servers, such
as Yanosh or the HLC Cluster. Production is for stable deployments actually
used, while Staging is a pre-production test environment. This can apply
either to services that require pre-testing before deployment, or for
services such as Minecraft that require a second environment to run on an
alternate port.

Dev is an alternate testbed envirionment meant to be used primarily by
development environments, such as Dreamweaver's Minikube. This is not
currently used, but is earmarked in case the need for differentiating
cluster hardware is required.

To apply the namespaces, first run the following for each namespace
needed on the cluster:
```
$ kubectl create -f eagleworld-namespace.json
$ kubectl create -f ew-staging-namespace.json
$ kubectl create -f ew-dev-namespace.json
```

Next, to define contexts we can use to manage the cluster, we need to
first get the cluster and admin user information, which can be found by
running this command:
```
$ kubectl config view
```

Once the cluster and name are retrieved, run the commands below for each
namespace needed:
```
$ kubectl config set-context eagleworld --namespace=eagleworld \
  --cluster=<CLUSTERNAME> --user=<USERNAME>
$ kubectl config set-context ew-staging --namespace=ew-staging \
  --cluster=<CLUSTERNAME> --user=<USERNAME>
$ kubectl config set-context ew-dev --namespace=ew-dev \
  --cluster=<CLUSTERNAME> --user=<USERNAME>
```

For example, creating the eagleworld namespace on minikube:
```
$ kubectl config set-context eagleworld --namespace=eagleworld \
  --cluster=minikube --user=minikube
```

Creating on a kubeadm-deployed Kubernetes cluster (such as Yanosh):
```
$ kubectl config set-context eagleworld --namespace=eagleworld \
  --cluster=kubernetes --user=kubernetes-admin
```

In order to set one of the new namespaces as the default namespace, use
the following command:
```
$ kubectl config use-context eagleworld
```

If you need to see which namespace you are using, run the following:
```
$ kubectl config current-context
```
